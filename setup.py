from setuptools import setup, find_packages


setup(
      name='slytherin',
      version='0.1',
      description='Useful but lonely functions and classes',
      url='',
      author='Idin',
      author_email='d@idin.net',
      license='GNU AGPLv3',
      packages=find_packages(exclude=("jupyter_tests", ".idea", ".git")),
      install_requires=['pandas', 'geopy', 'jellyfish', 'barbarian'],
      python_requires='~=3.6',
      zip_safe=False
)