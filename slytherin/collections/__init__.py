from .dictionary import Dictionary
from .has_duplicates import has_duplicates, get_duplicates
from .get_dict_product import get_dict_product
from .is_iterable import is_iterable
from .get_intersection import get_intersection
from .rename_dict_keys import rename_dict_keys
from .apply_on_dict_keys import apply_on_dict_keys
from .merge_dictionaries import merge_dictionaries
from .OrderedSet import OrderedSet
from .remove_list_duplicates import remove_list_duplicates

